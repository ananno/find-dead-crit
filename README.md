# Find Dead Crit

Interesting game based on grid. Nice problem for beginner in C.


## Problem Description
There is a 9x9 grid. The 9x9 grid is divided into 9 3x3 grids. A 3x3 grid is called a Crit. Each Crit has a name.
<br>The coordinate of the top left cell of the grid is (0, 0). The grid is shown below with each Crit’s name as follows:

* Grid 


<table>
  <tr>
    <td> </td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td>0</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td>1</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td>2</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td>3</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td>4</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td>5</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td>6</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td>7</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
  <tr>
    <td>8</td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
    <td> </td>
  </tr>
</table>
<br>

* Crits


<table>
  <tr>
    <td> </td>
    <td>0</td>
    <td>1</td>
    <td>2</td>
    <td>3</td>
    <td>4</td>
    <td>5</td>
    <td>6</td>
    <td>7</td>
    <td>8</td>
  </tr>
  <tr>
    <td>0</td>
    <td colspan="3" rowspan="3">SUN</td>
    <td colspan="3" rowspan="3">MERCURY</td>
    <td colspan="3" rowspan="3">VENUS</td>
  </tr>
  <tr>
    <td>1</td>
  </tr>
  <tr>
    <td>2</td>
  </tr>
  <tr>
    <td>3</td>
    <td colspan="3" rowspan="3">EARTH</td>
    <td colspan="3" rowspan="3">MARS</td>
    <td colspan="3" rowspan="3">JUPITER</td>
  </tr>
  <tr>
    <td>4</td>
  </tr>
  <tr>
    <td>5</td>
  </tr>
  <tr>
    <td>6</td>
    <td colspan="3" rowspan="3">SATURN</td>
    <td colspan="3" rowspan="3">URANUS</td>
    <td colspan="3" rowspan="3">NEPTUNE</td>
  </tr>
  <tr>
    <td>7</td>
  </tr>
  <tr>
    <td>8</td>
  </tr>
</table>
<br>


Each Crit has a prestige. The prestige of a Crit is the sum of all values inside the Crit. 
There are 9 values inside every Crit. A Crit which has the lowest prestige of all Crits is called dead Crit.
Initially, all elements of all 81 cells of the 9x9 grid are -1.

__Write a program in standard C to print the name of the dead Crit.__

## Input

There will be several lines of input. Each line will have 3 space separated integers:

`<row> <col> <value>`

Each input line will modify the content with `<value>` of corresponding `<row>, <col>` of the 9x9 grid. The `<row>` and `<col>` are from 0 to 8. The `<value>` is from -99 to 99.

## Output

After every input line, print out the name of the dead Crit.

* If there are multiple dead Crits, print the name of the dead Crit which has the lowest row number.
* If there are multiple dead Crits in the lowest row, print the name of the dead Crit which has lowest column number.

## Sample Input & Output Explanation

<table>
  <tr>
    <td>Sample Input</td>
    <td>Explanation</td>
    <td>Sample Output</td>
  </tr>
  <tr>
    <td>0 0 0</td>
    <td>Initially the prestige of all Crits is -9. This input changes the prestige of SUN to -8, making it the Crit with largest prestige. All other Crits still have the prestige -9. Among them, MERCURY has the lowest row and column number.</td>
    <td>MERCURY</td>
  </tr>
  <tr>
    <td>0 3 1</td>
    <td>After this modification to the grid, MERCURY’s prestige has increased. Each of the remaining 7 Crits has the same prestige, of which VENUS has the lowest row and column number.</td>
    <td>VENUS</td>
  </tr>
  <tr>
    <td>0 6 2</td>
    <td>Explanation is similar to the above.</td>
    <td>EARTH</td>
  </tr>
  <tr>
    <td>3 0 3</td>
    <td> </td>
    <td>MARS</td>
  </tr>
  <tr>
    <td>3 3 4</td>
    <td> </td>
    <td>JUPITER</td>
  </tr>
  <tr>
    <td>3 6 5</td>
    <td> </td>
    <td>SATURN</td>
  </tr>
  <tr>
    <td>6 0 6</td>
    <td> </td>
    <td>URANUS</td>
  </tr>
  <tr>
    <td>6 3 7</td>
    <td> </td>
    <td>NEPTUNE</td>
  </tr>
  <tr>
    <td>6 6 8</td>
    <td> This input will increase NEPTUNE’s prestige, making SUN’s prestige the lowest one.</td>
    <td>SUN</td>
  </tr>
  <tr>
    <td>3 3 -3</td>
    <td>MARS’ prestige is decreased so much that it becomes the lowest, making it the dead Crit.</td>
    <td>MARS</td>
  </tr>
  <tr>
    <td>0 3 -3</td>
    <td> Explanation is similar to the above.</td>
    <td> MERCURY</td>
  </tr>
</table>











