#include<stdio.h>
#include<stdlib.h>
#include<string.h>

int grid[9][9];
int prestige[3][3] = {{0, 0, 0}, {0, 0, 0}, {0, 0, 0}};
char crit_name[9][8] = {"SUN", "MERCURY", "VENUS", "EARTH", "MARS", "JUPITER", "SATURN", "URANUS", "NEPTUNE"};


void init_grid() {
    for(int row=0; row<9; row++) {
        for (int col=0; col<9; col++) {
            grid[row][col] = -1;
        }
    }
}
void init_prestige() {
    for (int row=0; row<3; row++) {
        for (int col=0; col<3; col++) {
            prestige[row][col] = 0;
        }
    }
}

void print_grid() {
    printf("\nGrid:\n");
    for(int row=0; row<9; row++) {
        for (int col=0; col<9; col++) {
            printf("| %d ", grid[row][col]);
        }
        printf("\n");
    }
}

void print_crits() {
    printf("\nCrits:\n");
    for(int i=0;i<3;i++) {
        for(int j=0; j<3; j++) {
            printf("%s\t", crit_name[(i*3)+j]);
        }
        printf("\n");
    }
}

void print_prestige() {
    printf("\nPrestiges:\n");
    for (int row=0; row<3; row++) {
        for (int col=0; col<3; col++) {
            printf("%d\t", prestige[row][col]);
        }
        printf("\n");
    }
}


void calculate_prestige() {
    init_prestige();
    for (int row=0; row<9; row++) {
        int r_div = row / 3;
        int r_mod = row % 3;
        for (int col=0; col<9; col++) {
            int c_div = col / 3;
            int c_mod = col % 3;
            prestige[r_div][c_div] += grid[row][col];
        }
    }

    print_prestige();

    int min_prestige = prestige[0][0];
    int min_row = 0;
    int min_col = 0;

    for (int row=0; row<3; row++) {
        for (int col=0; col<3; col++) {
            if (prestige[row][col] < min_prestige) {
                min_prestige = prestige[row][col];
                min_row = row;
                min_col = col;
                // printf("### Min <Prestige> <row> <col> : %d %d %d\n", min_prestige, min_row, min_col);
            } else if (prestige[row][col] == min_prestige){
                if (row < min_row) {
                    min_row = row;
                    if (col < min_col) {
                        min_col = col;
                    }
                }
            }
            // printf("Min <Prestige> <row> <col> : %d %d %d\n", min_prestige, min_row, min_col);
        }
    }
    printf("\nDead Crit: %s\n\n", crit_name[min_row*3+min_col]);
}

void print_all() {
    print_grid();
    print_crits();
    print_prestige();
}

void main() {
    
    init_grid();
    calculate_prestige();

    print_all();

    int row;
    int col;
    int value;
        
    while(1) {
        printf("\nEnter input [<row> <col> <value>] : \n");
        scanf("%d %d %d", &row, &col, &value);

        if(row > 8 || col > 8 || value < -99 || value > 99) {
            printf("-*-*-*- Invalid Input -*-*-*-\n");
            printf("Input : 0 <= row <= 8; 0 <= col <= 8; -99 <= value <= 99\n");
        } else {
            grid[row][col] = value;
            calculate_prestige();
            
        }

    }

}
